unit aria2c;
// http://sourceforge.net/projects/aria2/?source=typ_redirect
// http://aria2.sourceforge.net/manual/ru/html/aria2c.html
{ TODO : WTF, warning }
{$M+}

interface

uses
  System.SysUtils, System.StrUtils, System.Classes,
  Winapi.Windows, Winapi.Messages, // Vcl.Dialogs,

  System.JSON,

  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdHTTPHeaderInfo, IdHTTPWebBrokerBridge,

  JobsAPI;//, httpsend, synautil;

type
  RPC_listen_port = 1024..65535;  // http://aria2.sourceforge.net/manual/ru/html/aria2c.html?highlight=adduri#cmdoption--rpc-listen-port
  GID = string[16]; // http://aria2.sourceforge.net/manual/ru/html/aria2c.html?highlight=adduri#id17


/// <summary>
/// aria2c RPC server data
/// </summary>
  ProcessData = record
    hProcess: THandle;
    hThread: THandle;
  end;


/// <summary>
/// response XML, JSON: code (0 - sucessfuly) and message
/// </summary>

  TAriaResponse = record
    code: Integer;
    smessage : string;
  end;

TLogAppend = procedure(Message: string) of object;


TAria2 = class(TObject)
  protected
    Fexe: string;
    Fparams: string;
    // FSecret: string;
    FPort: RPC_listen_port;
    FprocessData: ProcessData;
    //FhProcess: THandle;
    //FhThread: THandle;
    //FDebugLines : TStrings;
    //Ftoken: TJSONValue;
    function RPC(const method: string; params: TJSONArray = nil): TAriaResponse;
    // procedure AddLog(s: string);
  public
    OnAppengLogMessage: TLogAppend; // for debug

    constructor Create(exename: string; exeparams: string ='' ; Port: RPC_listen_port = 6800);
    destructor Destroy; override;

    function addUri(uris: string; optionsKeyValue: TStringList = nil; position: Integer =0): TAriaResponse; // add new download

    function start(show: Boolean = False): boolean; // start server
    function stop: boolean; // stop server


  published
  {
    property exe: string read Fexe;
    property params: string read Fparams;
    property hProcess: THandle read FhProcess;
  }
//    property DebugLines : TStrings write FDebugLines;
  end;

// some helper functions
function ExecuteProcess(const EXE : String; const AParams: string = '';
  hJob: THandle = INVALID_HANDLE_VALUE; ShowWindow: Word = SW_SHOWNORMAL): ProcessData;
function SafeTerminateProcess(const hProcess: Cardinal; var uExitCode: Cardinal): Boolean;


implementation

function TAria2.RPC(const method: string; params: TJSONArray = nil): TAriaResponse;
var
  http: TIdHTTP;
  res, s: string;
  RequestBody: TStream;

  jsonRequest: TJSONObject;
  jsonParams: TJSONArray;
begin
  if params=nil then
     params:=TJSONArray.Create;

  Result.code:=-1;
  Result.smessage:='Unknown error';

  // base data
  jsonRequest:=TJSONObject.Create;
  jsonRequest.AddPair('jsonrpc', '2.0');
  jsonRequest.AddPair('id', 'qwer');
  // method
  jsonRequest.AddPair('method', 'aria2.'+method);

  // token
  jsonParams:=TJSONArray.Create;
  jsonParams.Add('token:'+inttostr(self.GetHashCode));

  jsonParams.Add(params);
  jsonRequest.AddPair('params', jsonParams);

  s:=jsonRequest.ToString;
  OnAppengLogMessage(s);
  //AddLog(s);

  jsonRequest.Free;

  RequestBody := TStringStream.Create(s, TEncoding.UTF8);
  http := TIdHttp.Create(nil);
  HTTP.Request.ContentType := 'application/json';
  http.HandleRedirects := False;
  http.ReadTimeout := 5000;

try
  try
    res:=http.Post('http://localhost:'+inttostr(FPort)+'/jsonrpc', RequestBody);
  except
    //
  end;
finally
    RequestBody.Free;
    http.free;
end;

  OnAppengLogMessage('res: '+ res);
end;


function TAria2.addUri(uris: string; optionsKeyValue: TStringList = nil; position: Integer =0): TAriaResponse;
var
  st: TStringList;
  arr: TJSONArray;
  s: string;
  i: Integer;
begin
  arr:=TJSONArray.Create;

  st:=TStringList.Create;
  st.Text:=ReplaceStr(uris, ';', sLineBreak);

  for I:=0 to st.Count-1 do
     begin
     s:=trim(st[i]);
     arr.Add(s);
     end;


  st.Free;
  Result:=RPC('addUri', arr);
end;

{$REGION 'start/stop/create/destroy/log...'}
{
procedure TAria2.AddLog(s: string);
begin
  if FDebugLines = nil then Exit;
  FDebugLines.Append(s);
end;
}
//procedure TAria2.

function TAria2.start(show: Boolean = False): boolean;
begin
  try
//  FprocessData:=ExecuteProcess(Fexe, Fparams + ' --enable-rpc', JobsAPI.hJob, StrToInt(IfThen(show, '1', '0')));
  FprocessData:=ExecuteProcess(Fexe, Fparams +
        ' --enable-rpc --rpc-secret ' + IntToStr(Self.GetHashCode)
      + ' --rpc-listen-port='+inttostr(FPort)
      + ' --console-log-level=error'
      , INVALID_HANDLE_VALUE, StrToInt(IfThen(show, '1', '0')));
  finally
    Result:=FprocessData.hProcess <> INVALID_HANDLE_VALUE;
  end;
end;

function TAria2.stop: boolean;
var
  iEC: Cardinal;
begin
  Result:=True;
  if (FprocessData.hThread=INVALID_HANDLE_VALUE) and (FprocessData.hProcess=INVALID_HANDLE_VALUE) then
    exit;

  iEC:=0;
  Result:=SafeTerminateProcess(FprocessData.hProcess, iEC);
  if Result then
    begin
    CloseHandle(FprocessData.hThread);
    CloseHandle(FprocessData.hProcess);
    FprocessData.hThread:=INVALID_HANDLE_VALUE;
    FprocessData.hProcess:=INVALID_HANDLE_VALUE;
    end;
end;


constructor TAria2.Create(exename: string; exeparams: string ='' ; Port: RPC_listen_port = 6800);
begin
  inherited Create;
  Fexe    :=exename;
  Fparams :=' '+exeparams;
  FPort   :=Port;
  //FDebugLines := nil;
  FprocessData.hProcess :=INVALID_HANDLE_VALUE;
  FprocessData.hThread  :=INVALID_HANDLE_VALUE;


  // secret Aria2c token
  //FSecret:=IntToStr(Self.GetHashCode);
//  Ftoken:=TJSONObject.Create(TJSONPair.Create('token', FSecret));
//  Ftoken:=TJSONValue.Create(fddfg);
//  Ftoken:='token:'+inttostr(Self.GetHashCode);
//  Ftoken:=TJSONPair.Create('token', FSecret);
end;

destructor TAria2.Destroy;
begin
  stop;
//  FSecret:='';
  //  Ftoken.Free;
  inherited Destroy;
end;

function ExecuteProcess(const EXE : String; const AParams: string = '';
  hJob: THandle = INVALID_HANDLE_VALUE; ShowWindow: Word = SW_SHOWNORMAL): ProcessData;
var
  SI : TStartupInfo;
  PI : TProcessInformation;
  AFlag: Cardinal;
begin
  Result.hProcess := INVALID_HANDLE_VALUE;
  Result.hThread := INVALID_HANDLE_VALUE;

  FillChar(SI,SizeOf(SI), 0);
  SI.cb := SizeOf(SI);

  SI.dwFlags := STARTF_USESHOWWINDOW;
  SI.wShowWindow := ShowWindow;

  // only for debug
  if ShowWindow = SW_SHOWNORMAL then
    begin
    SI.dwFlags := STARTF_USESHOWWINDOW or STARTF_USEPOSITION;
    SI.dwX:=1000;
    SI.dwY:=300;
    end;

  AFlag:=0;
  if hJob<>INVALID_HANDLE_VALUE then
     AFlag:=CREATE_BREAKAWAY_FROM_JOB;

  if CreateProcess(
     nil,
     PChar(EXE + ' ' + AParams),
     nil,
     nil,
     False,
     AFlag,
     // CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,
     nil,
     nil,
     SI,
     PI
     ) then
    begin
    Result.hProcess := PI.hProcess;
    Result.hThread  := PI.hThread;

    if (hJob<>INVALID_HANDLE_VALUE) and (PI.hProcess<>INVALID_HANDLE_VALUE) then
       AssignProcessToJobObject(hJob, PI.hProcess); // autoclose child process

    { close thread handle }
    //CloseHandle(PI.hThread);
    //CloseHandle(PI.hProcess);
    end;
//    ExitProcess
end;

function SafeTerminateProcess(const hProcess: Cardinal; var uExitCode: Cardinal): Boolean;
var
  iExitCode: Cardinal;
  hThread: Cardinal;
  iThreadId: Cardinal;
  hKernel: HMODULE;
  pExitProc: TFarProc;
begin
  // be optimistic -- True means the process is terminated
  Result := True;

  if not GetExitCodeProcess(hProcess, iExitCode) then
    // could not get an exit code--something is wrong
    Result := False
  else
    if iExitCode <> STILL_ACTIVE then
    // process as already terminated
    uExitCode := iExitCode
    else
    begin
    // get a pointer to ExitProcess

    hKernel := GetModuleHandle('Kernel32');
    pExitProc := GetProcAddress(hKernel, 'ExitProcess');

    // create a remote thread in that process
    Result := False;
    hThread := CreateRemoteThread(hProcess, nil, 0, pExitProc, Pointer(uExitCode), 0, iThreadId);


    if hThread <> 0 then
      begin
      WaitForSingleObject(hProcess, INFINITE);
      CloseHandle(hThread);
      Result := True;
      end;
    end;
end;
{$ENDREGION}

end.
